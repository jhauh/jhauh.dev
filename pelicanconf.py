#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

#AUTHOR = 'James'
SITENAME = "James' blog"
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/London'
DEFAULT_LANG = 'en'

STATIC_PATHS = [
            'extra',
            'img'
                ]
EXTRA_PATH_METADATA = {
            'extra/robots.txt': {'path': 'robots.txt'},
            'extra/favicon.ico': {'path': 'favicon.ico'},
                }

THEME = "theme"
#SITETAGLINE = "James' personal blog"
#FOOTERTEXT = ''

# Feed generation is usually not desired when developing
FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = None
FEED_ATOM = 'feeds/atom.xml'
FEED_RSS = 'feeds/rss.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Keep everything but pages in blog/
TAG_SAVE_AS = 'blog/tag/{slug}.html'
TAG_URL = 'blog/tag/{slug}'

TAGS_SAVE_AS = ''
TAGS_URL = ''

AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''

PAGE_SAVE_AS = '{slug}.html'
PAGE_URL = '{slug}'

CATEGORY_SAVE_AS = 'blog/{slug}.html'
CATEGORY_URL = 'blog/{slug}'

ARTICLE_SAVE_AS = 'blog/{slug}.html'
ARTICLE_URL = 'blog/{slug}'

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
