===================
 Let there be site
===================

:date: 20190730
:modified: 20191230
:slug: hello-world
:summary: I made a website
:category: Posts
:tags: website

.. role:: newthought

:newthought:`I read a lot of blog posts`, and I get a lot out of them.
Mostly it's tech-related stuff from my primary vices - Hacker News and
Reddit. Mostly it's a person who's done something, telling the world
about that thing. And mostly that's enjoyable to read. Now the time
has come for me to write one of my own, to tell the world about my
things, so here we are.

.. sidebar:: Why personal websites

   https://writing.markchristian.org/2019/04/29/personal-web-sites

   https://indieweb.org  
   
   https://www.jvt.me/posts/2019/07/22/why-website
   
   https://snarfed.org/2012-07-25_why_i_have_my_own_web_site

   http://dangillmor.com/2014/04/25/indie-web-important

   https://www.onebigfluke.com/2012/07/focusing-on-positives-why-i-have-my-own.html

Over my years of reading blogs I've been successfully and thoroughly
convinced that they're a good idea and that more people should have
them. My own reasons for making this one are to learn more about how the
web works and because it's become apparent to me that the world finally
deserves to hear more about what I think, but there are plenty of other
arguments for keeping a personal website:

#. Owning all your data is good
#. Being able to present what you like, how you like, is cool
#. Not being tied to any given platform seems better than being tied to
   one
#. It seems to be a right of passage for whatever reason
#. It might be a good way to motivate me to start and finish side 
   projects (do it for the blog)
#. You only YOLO once

There are countless blogs which do a much better job of expanding on
these points than I ever could, a few are listed.

I'll maintain a list of some of my favourites in a 
`blogroll <{filename}/pages/blogroll.rst>`_, and I'll try to emulate the
best parts of them in whatever I write. Having said that, there are 
plenty of bad ones out there too, so I'd like to start with a few goals
and ground rules for what I'd like to do (and avoid):

**Goals**

* Keep it snappy
* Be genuine
* Write good

**Anti-Goals**

* This is not a diary. In fact, let this post be the most self-indulgent
  one on here.
* Don't post just for the sake of it, or because it's been a while.
  There are no deadlines.
* Don't post rubbish about some hip and/or trendy tech just to impress
  or to say that you've used it.
* Related: This is not a LinkedIn. It's a personal blog, speak yo mind
  and write about whatever you want.

Here goes nothing.
