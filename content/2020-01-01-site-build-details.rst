Site build details
##################

:date: 20200101
:modified: 20200228
:slug: site-build-details
:summary: How this website is made
:category: Posts
:tags: website


.. role:: newthought

:newthought:`This post explains` how the site you are currently looking
at is created, generated, and published. It is intended to evolve with
the site as the site evolves, and offers an explanation (or defense) of
some of the design decisions found herein.


Current spec
============

The Tl;dr version is: 
`reStructuredText <https://docutils.sourceforge.io/rst.html>`_ and 
`Pelican`_ in a repo on `GitLab <https://gitlab.com/jhauh/jhauh.dev>`_
connected to `Netlify`_.


Requirements
============

:newthought:`I am not a web guy`, or a frontend guy, or a design guy.
I've never made a website before, so the fact that you can see and read
this now is a terrific accomplishment in my book. Everything else is
icing. Nonetheless, I started out in this endeavour with a few
requirements for the different elements of the site, beyond "words I
wrote appear on screen":

**A) From a reader's perspective**

1. Lightweight, quick to load
2. Focused on the content
3. Easy to navigate

**B) From my perspective**

1. Easy to write new posts
2. Easy to publish new posts
3. Easy to understand what's going on under the hood

**C) From a technical perspective**

1. Static
2. No JavaScript
3. Self-hosted

.. [1] https://idlewords.com/talks/website_obesity.htm

.. [2] https://redecentralize.org

\A) and B) are self-explanatory, but the underlying reason for C) is
that in my and everyone else's opinion, today's web is much too 
bloated\ [1]_ and centralised\ [2]_. And in order for me to continue to
hold such controversial positions, I should probably practice what I
preach by not contributing to the problem. On top of that, since one of
the main aims of this whole rigmarole is for me to figure out how the
Internet works, posting all this great chat to a third-party platform
would kind of defeat the purpose. 


Evaluating options
==================

.. [3] 

   https://www.staticgen.com/

   https://www.netlify.com/blog/2016/05/02/top-ten-static-website-generators/

:newthought:`As it turns out`, I'm not the first person to want to make
a website, so there already exists a rich ecosystem of tools,
frameworks, and guides for building them. I knew I wanted to use a
static site generator, so I did some homework reading through the many
lists and rankings of SSGs\ [3]_ until I got the gist of what the
main ones are, and what differentiates them. 

.. [4] 

   https://github.com/anthonynelzin/pelican-to-hugo

   https://gohugo.io/tools/migrations

In the end I chose `Pelican`_ due to the ability to write content in
reStructuredText (reST), a markup syntax I am very fond of, and to a
lesser extent because of the Python backend, a language I am very
familiar with. I expected that the backend language would only matter if
I were to write my own extensions or do something fancy, and so far I
have not felt the urge to do so. In any case it seems easy enough to
convert between the more popular ones\ [4]_, and if I ever need to move
to a Markdown-based framework, `Pandoc <https://pandoc.org/>`_ can
convert the old content.

.. [5] 
   
   https://github.com/getpelican/pelican-themes

   http://pelicanthemes.com

One of the practical differences between static site generators is the
set of themes available to each. It seems like Jekyll and Hugo users are
spoilt for choice in this regard, but Pelican still has a respectable
number of options to choose from\ [5]_. The design of the site is the
thing I'm least satisfied with, and is therefore the most prone to
tweaking and overhaul. :newthought:`Edit:` There's now a whole 
`separate post <{filename}/2020-02-10-site-design-details.rst>`_ on this
site's aesthetic design.

Once I had a folder full of HTML files, I needed a way to serve them to
the world. From what I've gathered, the old school way to do this is to
spin up a web server somewhere, put your files there, and point your
domain at it. You can do this all yourself or have a third party do all
or some of it for you, and automate it to varying degrees. There are too
many options to enumerate here.

I had heard good things about `Netlify`_, so that's what I went with for
starters. You connect it to your Git repo, tell it how to build your
site given the codebase, and then configure it to trigger a new build
when the repository is updated in some way. It is cheating a bit since
it abstracts a lot of the web parts away from you, but on the other hand
it covers B) single-handedly, and means that Git is the only tool I have
to interact with when publishing.


To do
=====

This is a mediocre website. There are many ways in which it could be
made less bad. If you have some ideas for things which would make it
less bad feel free to let me know. Here are the ideas I have so far:

- Stop using Netlify eventually so it loads everything from the one domain
- Consider including a different (serif?!) font


.. _Pelican: https://blog.getpelican.com
.. _Netlify: https://www.netlify.com
