=====================
 Raspberry Pi webcam
=====================

:date: 20210527
:slug: raspi-webcam
:summary: A Raspberry Pi makes a very good webcam
:category: Posts
:tags: hardware, raspberry pi

.. role:: newthought

:newthought:`I own too many Raspberry Pis.` So it's nice to get the
chance to put one to work for real, doing something useful. Lessens
the guilt.

.. [*]
   
   https://vsevolod.net/good-webcams

   https://reincubate.com/support/how-to/why-are-webcams-bad

Webcams these days aren't great\ [*]_. You can either: 

1. live with your laptop's webcam, 
2. use your (old) phone as a webcam with an app like DroidCam, 
3. spend ££ on a dedicated webcam which might be a bit better, 
4. spend £££ on a high-end webcam which will probably be better, 
5. or spend £££(£?) on an actual camera and all the cables and fake
   battery packs to turn it into a webcam which will definitely be much
   better. 

Or you can go down the Raspberry Pi webcam route (££), and end up with a 
quality image and a webcam you love all the more because you made it 
yourself.

.. sidebar:: Matebook X Pro webcam
   :class: titleless align-left

   .. figure:: ../img/mxp_webcam.jpg
      :width: 100%
      :figwidth: 100%
      :alt: Great for nasal inspections
      :align: left

      Matebook X Pro webcam

:newthought:`At time of writing`, my personal laptop is a Huawei
Matebook X Pro. In my opinion it's the best designed laptop on the
market today. Huawei got it so right (or are so lazy) that they haven't
changed the form factor in the four years since, with newer versions
mainly just updating the processor to the latest generation. It does
however have a terrible webcam, both in terms of image quality and
positioning. Good for privacy, no good for lockdown. So that's
option **1** down.

I moved on to option **2**, and after testing a couple of different
apps\ [*]_ eventually settled on `DroidCam`_. It works much better than
I expected, and the image quality of my very old phone (Galaxy S6) is
far better than any laptop webcam I've seen. All in all very
impressive, but it's not a perfect solution:

.. [*] I tested DroidCam and `Iriun`_. I hear `EpocCam`_ is also good if
   you have an old iPhone.

- It requires that you install phone-specific device drivers and run a 
  client on the computer you're connected to. This isn't always possible
  on work computers, and there is currently no Mac client (there's an
  OBS way but that's still another thing to install).
- For 720/1080p output the app needs to connect to the Internet/Play 
  Store to validate the license periodically. This is a pain because 
  ideally your webcam wouldn't require WiFi and you would keep it on
  airplane mode. 
- If this is going to be a permanent fixture, you have to figure out all
  of the other phone things, like turning the screen off while staying
  awake, going to sleep when not connected, battery limits etc.

I could have made peace with all of the above. I mean, who *doesn't*
love spending way longer than you wanted to trying to set up Tasker? The
real reason for abandoning DroidCam was: 

- The USB port of my old phone was too temperamental and kept spitting
  out cables

Moving down the list to option **3** proved impossible during lockdown
where the price of peripheral hardware spiked (example below), if you
could find any in stock at all. This also affected higher end webcams
to an extent (option **4**), but I had aleady mentally ruled out
spending £N00 on a webcam as being too far away from the `efficient
frontier`_ to consider.

.. figure:: ../img/stonks.png
   :class: fullwidth
   :alt: Price history for an ancient webcam
   :align: left

   "Diamond hands?" / "I learned Gimp just to make this and it was completely worth it"

Halfway through doing my homework on option **5**, wondering if there
might be an old Sony mirrorless on eBay, the `showmewebcam`_ project
popped up and swept me off my feet. It's custom firmware for the
Raspberry Pi to turn it into a generic USB webcam. Coupled with the
relatively new High Quality Camera Module for the Raspi, the demos and
examples showed off some excellent video quality. In one of `Dave
Hunt's videos`_ on the subject, he likens the project to LibreELEC,
whose tagline is "*Just enough OS for Kodi*". Showmewebcam is
essentially "*just enough OS for a webcam*".

.. sidebar:: BOM
   :class: titleless align-left

   ================ =======
   BOM              
   ================ =======
   Raspi Zero       £4 
   HQ camera        £50
   Lens             £25
   Mounting bracket £3
   Mini ribbon      £4
   **Total**        **£86**
   ================ =======

   TODO: Learn what affiliate links are

:newthought:`Being completely sold` on the project, I ordered some extra
hardware to pair with one of my many unemployed cupboard Pis. The only
decision to be made was which lens to use. I chose based on `this
mysterious line`_ in the project's Wiki. Once it all arrived, it was
straightforward to put together with a combination of the `official
guide`_ and `Jeff Geerling's video`_. Eventually I took it apart and
rebuilt it a much better way, see the appendix at the bottom for
how/why. After using it for five months, here are a collection of my
thoughts and experiences:

It works really well. I've used it on Linux, MacOS, and Windows, and it
just worked on all of them with no fuss. It boots and is ready to use
in seven seconds, and they've even set it up so the integrated LED on
the Pi flashes when it's ready to go, and turns on when the camera is
in use. I've used it across a number of applications (Teams, Zoom,
Discord, Skype, Photo Booth, VLC), sometimes for 6+ hours at a time,
without any slowdown, stuttering, or lag. It's handled being
unceremoniously disconnected very frequently. Overall a top job A+
10/10 would recommend.

In terms of image quality it's similarly impressive. The image produced
is sharp, clear, and has a nice natural bokeh you don't get with
smaller integrated webcams. Here is a picture of me to show the quality:

.. figure:: ../img/bear.jpg
   :class: fullwidth
   :alt: Sample picture quality
   :align: left

   Sample picture quality

Tried to find something sharp and something textured. Even with the poor
contrast with the background it looks great. I haven't done any
post-processing on the above, or optimised the camera for stills, just
took a snap using Photo Booth. It's representative to my eye.

Note that the lens I chose is quite zoomy, I would only recommend it if
you plan on sitting at least 70cm away (i.e. mounted on the wall behind
your monitor, not on the monitor itself). `The Wiki`_ has good
suggestions for lenses, though the official Raspberry Pi 6mm lens
doesn't seem nearly as bad as it's made out to be.

If all that hasn't convinced you to DIY, here are some other aspects I
find compelling:

- **No microphone** (a good thing)
  
  Webcam mics are never good.

- **Fixed focus** (another good thing)

  That fuzzy focus-finding thing some cameras do every 10 seconds is
  annoying. My sitting position is fixed, so should my focus be.

- **Plug and play**
   
  No drivers to download, no extra software to install, just boots in 7 seconds and blinks to let you know it's ready.

.. tip:: 
   Setting the bitrate to the maximum value in ``camera-ctl``
   improved the quality for me. 

- **Device-level camera settings**

  The bundled ``camera-ctl`` utility lets you
  mirror/rotate/sephia/whatever the video feed that gets passed to
  applications, so you don't have to rely on the video conference
  software du jour's camera tricks (although you can obviously apply
  those on top, e.g. background blur).

- **Big lenses**

  Your stupid face is `more attractive at longer focal lengths`_. 

- **Hackable, upgradable**

  Although the project looks like it's mostly done (you love to see it),
  there are still updates and pull requests open and trickling in.
  Updating is just a case of writing the new image to the SD card.

Overall I would 100% recommend the setup to anyone who is in the market
for a webcam. It's nice to see a Raspberry Pi project that is
legitimately competitive in its domain, and not just a toy or
for-the-sake-of-it endeavour. Maybe there's hope for the other Pis in
my drawer.

Appendix
--------

Though it now works on the RPi4, the Zero has enough horsepower for
720p30 (the current highest output quality of the firmware), and
requires much less energy. I'm 100% guessing now but I'm not sure just
any USB port can provide enough power to run the RPi4. 

You can usually find the acrylic mounting bracket anywhere that the HQ
camera is available. The default orientation of the bracket is great if
you have tripod or something to mount the camera to, but my plan was to
strap it to a pegboard on the wall. In order to make the unit a bit
neater, I made two changes to the default:

1. Rotate the camera unit 90° so the mounting bit doesn't stick out one
   side. This is possible because the HQ Camera module's mounting holes
   are square. This also means the camera ribbon doesn't have to bend
   around from the short side of the Pi to the long side.

2. Flip the Pi so the ports are on the inside. This allows the Pi to go
   flatter against the wall, protects the ports more, and most
   importantly means the camera ribbon connects with a fold rather than
   a twist. 

Here's what that looks like:

.. figure:: ../img/webcam_rebuilt.jpg
   :class: fullwidth
   :alt: Going off script on the webcam assembly
   :align: left

   Better way to assemble the webcam



.. _CNET: https://www.cnet.com/reviews/huawei-matebook-x-pro-review
.. _DroidCam: https://www.dev47apps.com
.. _Iriun: https://play.google.com/store/apps/details?id=com.jacksoftw.webcam
.. _EpocCam: https://www.elgato.com/en/epoccam
.. _efficient frontier: https://en.wikipedia.org/wiki/Efficient_frontier
.. _showmewebcam: https://github.com/showmewebcam/showmewebcam
.. _Dave Hunt's videos: https://www.youtube.com/watch?v=idD61VOJ024
.. _official guide: https://thepihut.com/blogs/raspberry-pi-tutorials/zero-mounting-plate-for-high-quality-camera-assembly-guide
.. _Jeff Geerling's video: https://www.youtube.com/watch?v=8fcbP7lEdzY
.. _this mysterious line: https://github.com/showmewebcam/showmewebcam/wiki/Lenses/_compare/f77d620c690790b29de6d3f628309bf744360228#diff-15ec48b17a37b5ac11bcd9358717872d3acfb7441d12bc6d12bb987b01051485R12
.. _The Wiki: https://github.com/showmewebcam/showmewebcam/wiki/Lenses
.. _more attractive at longer focal lengths: https://duckduckgo.com/?t=ffab&q=focal+length+face+effect&iax=images&ia=images