Site design details
###################

:date: 20200210
:modified: 20200228
:slug: site-design-details
:summary: Why this website looks like this
:category: Posts
:tags: website


.. role:: newthought

:newthought:`The thing about starting a blog from scratch` is that you
inevitably end up spending the first N posts talking about the blog
itself, because it's the thing you're currently working on. And who am I
to break from tradition? Hopefully this will be the last one for a
while.

.. [*] See https://en.wikipedia.org/wiki/Halo_effect


.. [*] A formerly technical manager once explained their role to me in a
   similar way |--| if they could improve the productivity of the people
   under them by y%, that would amount to a larger gain than their 
   potential individual contribution. Seems like a nice way to frame it.

:newthought:`A e s t h e t i c s` matter. More than they should, more 
than some prefer to admit. But they do. The mental model I have for 
appearances is that they act as a sort of multiplier on the real 
stuff\ [*]_, so me working on making this site look good is kind of like
working on all its posts at once, making them all x% better\ [*]_.
Understandably, I'd like my multiplier to be greater than one, so this
post discusses the work that I did and how I arrived at the thing you're
looking at right now. 

.. [1] 

   http://bettermotherfuckingwebsite.com

   https://evenbettermotherfucking.website

   https://bestmotherfucking.website

   https://thebestmotherfucking.website

   https://thebestmotherfuckingwebsite.co

:newthought:`The main experience` I have with making digital things look
nice is in the three 'P's; Plots and PowerPoints. Neither are
particularly helpful here, where the canvas itself is the focus. But we
all have to start somewhere, so I started here:
https://motherfuckingwebsite.com (SFW, promise). This site and all of
the rebuttal sites it inspired\ [1]_ turned out to each be a rung on the
ladder of learning basic HTML and CSS. They all sell the idea that "less
is more" very well, and mostly just haggle over the definition of
"less". Highly recommend the journey.

Then I found something which actually did link back to plots:
`Tufte CSS <https://edwardtufte.github.io/tufte-css/>`_. This framework
adapts the styles Edward Tufte employs in his books and handouts (about
how best to visualise and display data) for use with web articles. One 
of Tufte's most famous works |--| a must-read for anyone who
is in the business of creating plots and showing them to others to
convince them of things |--| is 
`The Visual Display of Quantitative Information <https://www.edwardtufte.com/tufte/books_vdqi>`_.
:title:`Tufte CSS` and the adaptations which came before it\ [2]_ are 
essentially "The Visual Display of 
':title:`The Visual Display of Quantitative Information`'".

.. [2] 
 
   https://tufte-latex.github.io/tufte-latex

   http://rmarkdown.rstudio.com/tufte_handout_format.html


From these resources and from the aesthetics of 
`my fave blogs <{filename}/pages/blogroll.rst>`_, I came up with a list
of characteristics I wanted this site to possess:

- Text only, no superfluous images
- No sticky headers, pop-ups, or anything else that gets in the way
- A sweet sweet Tufte-like sidebar
- Reasonably responsive, i.e. renders nicely on mobile too
- Lightweight, i.e. default fonts, no JavaScript
- Easy on the eye, i.e. colour contrast, text width, sizing

.. [3]
   
   https://github.com/getpelican/pelican-themes

   http://pelicanthemes.com

   Note that at the time of writing, the second link is currently
   unmaintained, and has not been updated in over 2 years. So digging
   through the repo is more work, but there are more (and more recent)
   themes in there.

.. [4]

   https://github.com/search?q=pelican+theme

   https://gitlab.com/search?&search=pelican+theme


Now all I had to do was find a theme that ticked all these boxes. I
searched high\ [3]_ and low\ [4]_, but couldn't find anything ready-made
which *quite* matched. I resigned myself to taking :title:`Tufte CSS`
and building a Pelican template around it from scratch, and had begun
down that path when some searching for similar projects yielded 
`B-side <https://hugo-b-side-demo.netlify.com>`_. Tufte-based, minimal,
and best of all, built with reStructuredText in mind. My heart was set.
The only problem |--| it was a theme built for Hugo, not Pelican.

Instead of doing the simple thing (porting a relatively empty site to
Hugo), I decided to do the complicated thing, and ported the theme to
Pelican. I can't really justify this choice; there's nothing on this
site which couldn't be accomplished in Hugo, and it certainly would have
been quicker to go the other way. The best post hoc rationalisations I
can come up with now are that it was a good means to gain a thorough
understanding of how Pelican works, and to learn the
`Jinja <https://jinja.palletsprojects.com>`_ templating language. The 
theme itself can be found 
`here <https://gitlab.com/jhauh/pelican_b_side>`_, along with an
accompanying 
`demo site <https://jhauh.gitlab.io/pelican_b_side_demo>`_.

:newthought:`You may have noticed` that what you are looking at right
now is not an exact match of either demo site linked above; you are very
astute. The port was intended to be a faithful rewrite of the original,
but I've made a few tweaks for its use here. These include using default
fonts, resizing things a little, and forgoing the image badge in the
header. As a result, the whole site weighs less than 500kB after Netlify
does all of its  minifying and optimising. While there's always room for
improvement,  this current theme fulfils my above requirements, and so I
must now  avoid the temptation to incessantly fiddle with it (as I have
already done for far too long), and instead turn my attention back to
writing, or doing things worth writing about.


.. |--| unicode:: U+2013   .. en dash
