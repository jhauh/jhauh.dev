Building a split keyboard
#########################

:date: 20200616
:slug: keyboard-build-logic
:summary: Welcome to the wonderful world of what I type words with
:category: Posts
:tags: hardware, build log, keyboard

.. figure:: ../img/keyboard.jpg
   :alt: Iris split keyboard
   :class: fullwidth

.. role:: newthought

:newthought:`Ain't she a beauty?` Feast your eyes on the product of a
few weeks of research, several months of patience, and a day of
nervous soldering. At this point, the uninitiated reader might be
wondering what would |--| nay, what *could* |--| possess someone to
create such a convoluted and contorted contraption. Fear not, for this
post seeks to initiate. But beware, it may also serve to indoctrinate.
So continue at your (wallet's) peril, and learn of the irrefutable logic
leading to the image above.

In other words, I built the keyboard above recently. This post presents
the logical journey which led to that decision, and provides some
analysis of its impact on my daily typing, and a review of the board
itself. The intended audience is someone who:

- might know about the existence of mechanical keyboards already, but
- hasn't had much or any exposure to split keyboards, and
- isn't aware of the flourishing open-source hardware ecosystem of input
  devices


The why
=======

:newthought:`My job consists` of quite a lot of time in front of a
keyboard, typing. I'd like to be able to say it's mostly writing code,
building models, and generally being a hackerman, but in truth most of
the keys I press are entered into emails and chats rather than Vim.
Either way, I do a non-negligible amount of typing on a daily basis. And
until now, I've done that typing on whatever instrument my employer
provided me, whether that be a laptop keyboard, or a full-sized Logitech
or Microsoft board. Until the past year or so that was perfectly
adequate and didn't really cross my mind as a point of configuration.
Sure, I would occasionally coo over colleagues' mechanical keyboards,
and complain about being *forced* to use laptop keyboards where the
``Fn`` and ``Ctrl`` keys had seemingly castled, but I lacked any real
drive to optimise my setup.

Motivation arrived in the form of creeping discomfort while typing for
long periods, stemming from a then-untangleable combination of old age
and poor ergonomics. Accepting that I was sadly unable to control the
former, I turned my attention to the latter.


The what
========

.. sidebar:: Elbow & shoulder rotation
   :class: titleless align-left

   .. figure:: ../img/keyboard_posture.svg
      :width: 100%
      :figwidth: 70%
      :alt: Keyboard ergonomics comparison
      :align: center

      "March of Progress 2" / "Me and the boys"


:newthought:`OK, so now I was in the market` for a keyboard; one with an
untwistable stomach. But what would actually help? I read  `a blog`_
which blamed all of my woes on the mouse being too far to the right due
to the number pad, and proclaimed that the answer lay in tenkeyless (aka
TKL) keyboards. This would mean the mouse could live closer in, thus
reducing the amount of external rotation at the shoulder/elbow. Sounds
reasonable.

As I searched for the perfect TKL I did some cursory searching for
ergonomic keyboards, and discovered to my surprise that it wasn't just
external rotation I should be eradicating from my working life, but
*internal* rotation as well\ [*]_. To accomplish this, many
ergonomic keyboards split the board into two distinct halves to allow
for a more neutral posture when typing. There were all kinds of options;
some which kept the whole unit together, like the classic  `Kinesis
Advantage 2`_, and others which separated entirely, allowing the user to
position their hands however they pleased, eg. `ErgoDox EZ`_.

One model which caught my attention was the `Ultimate Hacking Keyboard`_
(UHK). A kickstarter which at the time was beginning to bear fruit, it
seemed like a great call. It had plenty of authentic reviews heaping
praise on it, and the only `negative review`_ I could find on it was
oddly reassuring (see the discussion with one of the creators in the
comments). But it was expensive, so before I pulled the trigger I did
lots of research, and three things stuck out about it:

- `Tenting`_ is a thing
- Its uncommon key sizes make it difficult to get keycaps for
- Uses its own software for mapping keys, not QMK


The last point piqued my interest. `QMK`_ is open source, hackable
firmware for input devices, and it sounded right up my street. This
prompted the question; which boards *did* use QMK? To which the answer
was "loads of them", and all of a sudden there were dozens to choose
from, each with their own unique characteristics and reasons for
existing. Most if not all of these keyboards were completely open
source, with their design files freely available. I got aquainted with
all the new terminology before me, and attempted to pick a favourite.
Here are all the degrees of freedom which I considered, ordered by
importance:


.. sidebar:: Stagger
   :class: titleless align-left

   .. figure:: ../img/stagger.svg
      :width: 100%
      :figwidth: 75%
      :alt: Moves like stagger
      :align: left

      Row-staggered/Ortholinear/Column-staggered


#. **Number of rows**: Your options are generally three (no dedicated
   number row) [e.g. `Corne`_], four (with number row) [e.g. `Lily58`_],
   or five (number row above + modifier keys below) 
   [e.g. `Ergodox EZ`_].

#. **Key layout**: Row-staggered (i.e. standard, the way 99% of 
   keyboards are) v ortholinear (a regular rectangle) v column-stagger 
   (keys directly over each other, but not directly beside each other)

#. **Thumb cluster layout**: How many thumb keys, and where are they
   positioned. Can be placed under the palm [e.g. `Nyquist`_], or 
   further afield [e.g. `Pteron`_]. 

#. **Extras**: Jazzy features like rotary encoders, OLED screens, 
   per-key LEDs, or even wireless connections.

After some consideration I decided to go for something with four rows,
(couldn't give up the number row), column-stagger (to hopefully make
touch typing easier), and the option of adding tenting and some fancy
extras down the line. This narrowed the choice to two options; the Iris
and Lily58 Pro. In the end the Iris won out, mostly because of
availability, but also due to its revision 4 being released with most of
the components built in to the PCB, meaning less soldering (and
therefore opportunity for error) for a noob like me. Rev 4 also means
that the design has matured somewhat, with many tweaks and refinements.
And USB-C is always a welcome addition.

For those of you still reading, I hope the above journey adequately
explains the reasoning behind building the split keyboard at the top of
the page. But as I write this post using it, after having used it for a
couple of months, I must appraise the decision I made long ago.


The impact
==========

:newthought:`Objectively, after` ~3 months I'm still slower on split
than I am on traditional keyboards, when pace is the game (TY dgrov for
the inspo!). I spent a lot of time on `TypingClub`_, which I would
recommend wholeheartedly, and logged my daily mean WPM:

.. figure:: ../img/wpm.png
   :alt: WPM over time
   :width: 100%

Subjectively, it definitely feels more comfortable. The split really
forces touch typing over hunt and peck, and the column-stagger makes it
easier than on my laptop keyboard. I can type for longer without feeling
any strain, and the thumb cluster means my thumbs do a lot more work,
and pinkies do less, which is a win in my book.


Keymap/layout
=============

:newthought:`The board came` with a sensible default layout, but I
eventually plucked up the courage to flash it with one of my own making:

.. figure:: ../img/layout.png
   :alt: Personalised keymap
   :class: fullwidth

QMK is easy to use and allows for all kinds of customisations; the setup
above barely scratches the surface of what is possible. One feature
worth mentioning is the `Mod-Tap`_ buttons set up on the upper thumb
keys; when tapped, they act as regular ``=`` and ``-`` keys, but when
held down they become ``Shift`` and ``Ctrl`` respectively. Functionality
like this makes it possible to access all the keys you need on a smaller
board such as the Iris, and knowing it exists makes the idea of building
an even smaller board less scary. You can find my layout files in the
`QMK GitHub repo`_.


Reflections
===========

:newthought:`It's nice to be able to` reprogram board as I please, and
know that it'll work on any computer it's plugged into without needing
to install additional software. From work Windows laptops to a Rasberry
Pi, I haven't encountered any issues so far. The flash-test-iterate
feedback loop is quick and painless.

Another dimension of the board I've failed to mention thus far is its
mechanical switches. I went with `Zealios Zilents`_ (tactile silent) in
the hopes that I might be able to get away with using it in an office
environment. They're about as loud as my laptop keyboard. They've also
been enjoyable to type on, though I daresay my unrefined digits would
think the same of any mechanical switch. It's worth noting that at no
point was "mechanical" ever made an explicit requirement; it just so
happens that a majority of the specialist, ergonomic, or niche keyboards
considered use mechanical switches, so that's where I ended up.

The community/ecosystem seems to be flourishing. When I started out in
the summer of 2019, the subreddit `/r/ErgoMechKeyboards`_ didn't exist
yet, but now it's a real hub for this stuff. People are designing their
own boards, and making them available to others (both the design files
so they can make the boards themselves, but also through selling PCB
kits and preassembled groupbuys). `Kyria`_ is a recent example at the
time of writing (also, the creator's `first ever build log`_ inspired me
to write this post on my own experience, it's very readable). There are
also more exotic creations popping up, with made-to-measure `Dactyl
Manuforms`_ being the obvious example, but also `open source
trackballs`_, and even `integrating trackballs into Dactyl Manuforms`_.

All in all I'm glad to have stumbled upon this little corner of the
Internet. As a beginner I found it accessible and open; people are happy
to share layouts, case files, build logs, and anything else you might
need. The project was that nice mix of hardware and software we're all
looking for, and there's a lot further you can take it if you're so
inclined. Overall I'm chuffed with how it turned out, and would
recommend anyone who spends their days in front of a computer to give it
a try.


Appendix/Mini-thoughts
======================

*Keycap profiles* are another decision point. Uniform profiles like DSA
are easier to align their legends with reality on small split keyboards
because you can put any key on any row, and are usually recommended for
this reason. I went with a Cherry profile based off of an offhand remark
in a Hackernews or Geekhack thread which I've since lost that flat
profiles were originally designed and intended to be used on
slanted/rounded mounts, and I wanted something relatively low profile. 

*Tenting* seems like the next step up in ergonomics for me. There are a
number of different leg attachments for keyboard cases, but you can also
achieve a small inclination using rubber feet.

*/r/MechanicalKeyboards* has grown into a huge subreddit. From what I
can tell, it's more commercialised than `/r/ErgoMechKeyboards`_, and
there's a lot more showing off what you bought rather than what you
built. It's still a good source of info for switches, keycaps, and other
general keyboard things, but it definitely steers you more towards the
high-end, so be wary of that.


Useful resources
================

A great tool to compare different board layouts: https://jhelvy.shinyapps.io/splitkbcompare

A great list of split keyboards: https://github.com/diimdeep/awesome-split-keyboards

The great history of the Iris keyboard: https://medium.com/@keebio/lewis-ridden-and-the-story-behind-the-iris-7a70b03cfb80

.. [*] https://academic.oup.com/ptj/article/81/4/1038/2829527


.. |--| unicode:: U+2013   .. en dash

.. _a blog: https://www.forrestthewoods.com/blog/why_your_keyboard_is_bad_for_your_body
.. _Kinesis Advantage 2: https://kinesis-ergo.com/shop/advantage2
.. _ErgoDox EZ: https://ergodox-ez.com
.. _Ultimate Hacking Keyboard: https://ultimatehackingkeyboard.com
.. _negative review: https://www.jwz.org/blog/2018/08/ultimate-hacking-keyboard
.. _Tenting: https://deskthority.net/wiki/Ergonomic_keyboard#Tenting
.. _QMK: https://qmk.fm
.. _Corne: https://github.com/foostan/crkbd
.. _Lily58: https://github.com/kata0510/Lily58
.. _Nyquist: https://keeb.io/products/nyquist-keyboard
.. _Pteron: https://github.com/FSund/pteron-keyboard
.. _TypingClub: https://www.typingclub.com
.. _Mod-Tap: https://beta.docs.qmk.fm/using-qmk/advanced-keycodes/mod_tap
.. _QMK GitHub repo: https://github.com/qmk/qmk_firmware/tree/master/keyboards/keebio/iris/keymaps/fluffactually
.. _Zealios Zilents: https://zealpc.net/products/zilents
.. _/r/ErgoMechKeyboards: https://www.reddit.com/r/ErgoMechKeyboards
.. _Kyria: https://blog.splitkb.com/blog/introducing-the-kyria
.. _first ever build log: https://thomasbaart.nl/2018/11/16/my-first-custom-keyboard-an-iris-build-log
.. _Dactyl Manuforms: https://github.com/tshort/dactyl-keyboard
.. _open source trackballs: https://www.ploopy.co
.. _integrating trackballs into Dactyl Manuforms: https://medium.com/@kincade/track-beast-build-log-a-trackball-dactyl-manuform-19eaa0880222