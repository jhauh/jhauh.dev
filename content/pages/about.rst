About
#####

:date: 20190730
:slug: about
:summary: Meet James


.. sidebar:: Me
   :class: titleless align-left

   .. figure:: img/stickman.png
      :width: 100%
      :figwidth: 100%
      :alt: Meet James
      :align: center
      :target: https://commons.wikimedia.org/wiki/Category:Stick_figures#/media/File:Survey_administration_symbol.svg


Hello I'm James, a data science guy from Ireland. Currently I'm based
in Belfast.

This blog is the place where I put anything I do that I think might be
useful or interesting to others. You can subscribe via 
`RSS <https://www.jhauh.dev/feeds/rss.xml>`_ or 
`Atom <https://www.jhauh.dev/feeds/atom.xml>`_ to get new posts when I 
put them up.

Take a look at my public repos on 
`GitLab <https://gitlab.com/jhauh>`_ and 
`GitHub <https://github.com/jhauh>`_. 

To get in touch, ``jh`` [ at ] ``ughey`` [ dot ] ``ie``

**License**

The content for this site is 
`CC-BY-SA <https://creativecommons.org/licenses/by-sa/4.0/>`_. The code
for this site is `MIT <https://opensource.org/licenses/MIT>`_. 
