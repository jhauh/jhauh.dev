import matplotlib.pyplot as plt
from matplotlib.patches import FancyBboxPatch
import pandas as pd
import seaborn as sns

sns.set_context('talk')
plt.style.use('seaborn-whitegrid')

wpm = (
    pd
        .read_csv('WPM.csv')
        .reset_index()
        .rename(columns={'index': 'Day of Practice'})
)

fig, ax = plt.subplots(figsize=(8, 5))
ax.set_xlim(0, 40)
ax.set_ylim(0, 70)
hunt = FancyBboxPatch(
    (0, 55),
    width=40,
    height=10,
    color='salmon',
    alpha=0.5,
    boxstyle='sawtooth',
    label='Hunt & Peck'
)
ax.add_patch(hunt)
ax.plot(
    wpm['Day of Practice'], wpm['WPM'],
    color='blue', label='Iris Touch Typing'
)
ax.set_xlabel('Day of Practice')
ax.set_ylabel('WPM')
ax.legend(loc='lower right')
fig.tight_layout()

fig.savefig('wpm.png', transparent=True)

